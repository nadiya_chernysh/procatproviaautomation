﻿using ProcatAutomation;
using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;
using ProcatAutomation.Elements;

namespace ProcatAutomation.Pages
{
    public abstract class BasePage
    {
        private Browser _browser;

        public BasePage(Browser browser)
        {
            _browser = browser;
        }
    }
}
