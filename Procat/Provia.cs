﻿using E2ETests.Base;
using NUnit.Framework;
using ProcatAutomation.Pages;
using E2ETests.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E2ETests
{
    public class Provia : TestBase
    {
        [SetUp]
        public void Setup()
        {

        }

        [Test]
        public void BieterPortal_VeroffenlicheVerfahren_RegistrierungLinkAnfordernTest()
        {          
            //arrange
            ProviaVeroffentlicheVerfahrenPage verfahrenPage = new ProviaVeroffentlicheVerfahrenPage(browser);

            //act
            verfahrenPage.OpenVerofentlicheVerfahrenTab();
            verfahrenPage.OpenVerfahrenDetails();
            browser.Pause(1); //only for presentation purposes
            verfahrenPage.ClaimInterest();
            verfahrenPage.RegistrierungLinkAnfordern(""); //no input for email field in order to generate error
            browser.Pause(1); //only for presentation purposes

            //assert
            verfahrenPage.WaitForErrorMessage("field-validation-error");
        }

        [Test]
        public void BieterPortal_Angebotsabgabe_PreisangebotErstellenTest()
        {
            //arrange
            ProviaLoginPage loginPage = new ProviaLoginPage(browser);
            loginPage.Login(config.ProviaBieter, config.ProviaBieterPw);
            ProviaMeineVerfahrenPage meineVerfahren = new ProviaMeineVerfahrenPage(browser);

            //act
            meineVerfahren.OpenCurrentTender();
            meineVerfahren.GoToAngeboteSection();
            meineVerfahren.PreisangebotErstellen();
            meineVerfahren.FillBasisangebotData(5000000, 20000, 3);

            //assert
            //example of assertion to verify main points of data presented to the user, such a total price or pdf document
            //main interactions (actions) are verified by default, by the steps of automated test scenario
            meineVerfahren.VerifyGesamtpreis("5.170.600,00 €");

            //act
            meineVerfahren.UploadBestandteile();
            meineVerfahren.SelectSignature(outsideOfPlatform: true);
            meineVerfahren.UploadAngebotshauptteil();
            meineVerfahren.SaveOfferAndClose();
            //meineVerfahren.SubmitOffer();
            meineVerfahren.DeleteAngebotsHauptteil();
            meineVerfahren.SaveOfferAndClose();
        }

        [Test]
        public void BieterPortal_CheckDocument_Angebotsbestaetigung()
        {
            //arrange
            ProviaLoginPage loginPage = new ProviaLoginPage(browser);
            loginPage.Login(config.ProviaBieter, config.ProviaBieterPw);
            ProviaMeineVerfahrenPage meineVerfahren = new ProviaMeineVerfahrenPage(browser);

            //act
            meineVerfahren.OpenCurrentTender();            
            meineVerfahren.GoToVerfahrensverlauf();
            meineVerfahren.DownloadOfferConfirmation();

            //assert
            var content = ParsePdf($"{config.DownloadsLocation}Angebotsbestaetigung.pdf");            

            AssertPdfContent(content, new List<(string, string)>() {
                ("Angebotsabgabe Bestätigung", "Zeitstempel"),
                ("ID-Nr", meineVerfahren.GetCurrentTenderId()), 
                ("Status der Signatur", "Gemäß Einreicher außerhalb von ProVia signiert")
            });
        }
    }
}
