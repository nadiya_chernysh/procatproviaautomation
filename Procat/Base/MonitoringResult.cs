﻿using System;
using System.Collections.Generic;
using System.Text;

namespace E2ETests.Base
{
    public class MonitoringResult
    {
        public string Transaction { get; set; }        
        public string Status { get; set; }
    }
}
