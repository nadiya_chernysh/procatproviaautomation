using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using ProcatAutomation.Pages;
using E2ETests.Base;

namespace E2ETests
{
    public class ProcatSmokeTests : TestBase
    {
        [SetUp]
        public void Setup()
        {

        }                    

        [Test]
        [Ignore("")]
        public void OrderListTest()
        {
            CategoriesPage categoriesPage = new CategoriesPage(browser);

            categoriesPage.OpenOrderList();
            categoriesPage.FilterOrderByStatus();
            categoriesPage.ConfirmLastOrder();
        }

        [Test]
        [Ignore("")]
        public void CategoriesNavigationTest()
        {
            CategoriesPage categoriesPage = new CategoriesPage(browser);

            categoriesPage.OpenOrderList();
            categoriesPage.FilterOrderByStatus();
            categoriesPage.ConfirmLastOrder();
        }
    }
}