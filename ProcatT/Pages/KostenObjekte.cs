﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProcatAutomation.Pages
{
    public enum KostenObjekte
    {
        ÖBB_Infrastructure,
        ÖBB_Dienstleistungen,
        ÖBB_Personenverkehr
    }     
}
