﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace ProcatAutomation
{
    public class Browser
    {
        private string BaseAddress = "https://www.provia.at/bieterportal/";        
        private IWebDriver _webDriver;        

        private IWebDriver WebDriver
        {
            get
            {
                if (_webDriver == null)
                {
                    var options = new ChromeOptions();
                    //options.AddArgument("--no-sandbox");
                    //options.AddArgument("--headless");
                    _webDriver = new ChromeDriver(options);                    
                }
                return _webDriver;
            }
        }

        public void WaitForElementClickable(By selector, int timeout = 15)
        {            
            try
            {
                GetWebDriverWait(timeout).Until(ExpectedConditions.ElementToBeClickable(selector));
            }
            catch (ElementNotVisibleException e)
            {
                Logger.Log.Error(e);
                throw e;
            }
        }

        public void WaitForElementVisible(By selector, int timeout = 15)
        {
            try
            {
                GetWebDriverWait(timeout).Until(ExpectedConditions.ElementIsVisible(selector));
            }
            catch (ElementNotVisibleException e)
            {
                Logger.Log.Error(e);
                throw e;
            }

        }

        public IWait<IWebDriver> GetWebDriverWait(int timeout)
        {
            return new WebDriverWait(_webDriver, TimeSpan.FromSeconds(timeout));
        }

        public List<IWebElement> FindElements(By bySelect)
        {
            return _webDriver.FindElements(bySelect).ToList();
        }

        public IWebElement FindElement(By bySelect)
        {
            return _webDriver.FindElement(bySelect);
        }

        public void Goto(string url)
        {
            WebDriver.Url = BaseAddress + url;
        }

        public void Close()
        {
            WebDriver.Manage().Cookies.DeleteAllCookies();
            WebDriver.Quit();
        }

        public void Initialize()
        {
            Goto("login");
        }

        public void Pause(int timeout)
        {
            Thread.Sleep(timeout * 1000);
        }

        public TResult WaitForCustom<TResult>(Func<IWebDriver, TResult> expectedCondition)
        {
            var wait = new WebDriverWait(_webDriver, TimeSpan.FromSeconds(10));
            return wait.Until(expectedCondition);
        }

        public bool IsElementPresent(By selector, int timeout = 2)
        {
            try
            {
                WebDriver.FindElement(selector);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        public void SwitchToNewTab()
        {
            string newTabHandle = _webDriver.WindowHandles.Last();
            _webDriver.SwitchTo().Window(newTabHandle);

        }

        public void SwitchToModalWindow()
        {
            _webDriver.SwitchTo().ActiveElement();
            
            //string current = _webDriver.CurrentWindowHandle;
            //PopupWindowFinder finder = new PopupWindowFinder(_webDriver);
            //string newHandle = finder.Click(_webDriver.FindElement(By.ClassName(" ")));
            //_webDriver.SwitchTo().Window(newHandle);

            //switchToModalDialog(_webDriver, parent = _webDriver.CurrentWindowHandle);
        }             
               

        public string Title
        {
            get { return WebDriver.Title; }
        }

        public void Navigate(string url)
        {
            WebDriver.Navigate().GoToUrl(url);
        }
        

        public string TakeScreenshot(string directory)
        {
            var screenshot = ((ITakesScreenshot)WebDriver).GetScreenshot();
            var path = directory + "\\Screenshots" + "\\Screenshot_" + DateTime.Now.ToString("dd-MM-yyyy HH-mm-ss") + ".png";
            screenshot.SaveAsFile(path, ScreenshotImageFormat.Png);
            return path;
        }     
                

        public void WaitForTextToBePresentInElementValue(By selector, string text, int timeout = 5)
        {
            try
            {
                GetWebDriverWait(timeout).Until(ExpectedConditions.TextToBePresentInElementValue(selector, text));
            }
            catch (NoSuchElementException e)
            {
                Logger.Log.Error(e);
                throw;
            }
        }
        
        public void WaitForElementClickableByElement(IWebElement element, int timeout = 5)
        {
            GetWebDriverWait(timeout).Until(ExpectedConditions.ElementToBeClickable(element));
        }

        public void WaitForTextToBePresentInElement(IWebElement element, string text, int timeout = 5)
        {
            GetWebDriverWait(timeout).Until(ExpectedConditions.TextToBePresentInElement(element, text));
        }

        public void WaitForTextToBePresentInElementLocated(By selector, string text, int timeout = 15)
        {
            GetWebDriverWait(timeout).Until(ExpectedConditions.TextToBePresentInElementLocated(selector, text));
        }        

        public void WaitForElementNotVisible(By selector, int timeout = 5)
        {
            GetWebDriverWait(timeout).Until(ExpectedConditions.InvisibilityOfElementLocated(selector));
        }

        public void WaitForPageToBeFullyLoaded(int timeout = 5)
        {
            GetWebDriverWait(timeout).Until(d => ((IJavaScriptExecutor)d).ExecuteScript("return document.readyState").Equals("complete"));
            //GetWebDriverWait(timeout).Until(d => (Boolean)((IJavaScriptExecutor)d).ExecuteScript("return jQuery.active == 0"));
        }

        public void Maximize()
        {
            WebDriver.Manage().Window.FullScreen();
        }

        public void AcceptAlert()
        {
            WebDriver.SwitchTo().Alert().Accept();
        }

        public string GetCurrentUrl()
        {
            return WebDriver.Url;
        }

    }
}
