﻿using Autofac;
using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using E2ETests.Config;
using E2ETests.IocConfig;
using E2ETests.Utilities;
using log4net.Config;
using Newtonsoft.Json;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using ProcatAutomation;
using ProcatAutomation.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace E2ETests.Base
{
    public class TestBase
    {
        protected Browser browser;
        protected ExtentReports extent;
        protected ExtentTest test;
        protected PdfParser parser;
        private string path;        
        protected List<MonitoringResult> results = new List<MonitoringResult>();
        protected MailClient mailClient = new MailClient();
        protected static IContainer Container => Ioc.Container;
        protected static E2ETestsConfig config => Container.Resolve<E2ETestsConfig>();

        [OneTimeSetUp]
        public void BeforeSuit()
        {
            Ioc.BuildContainer();

            XmlConfigurator.Configure();

            var dir = TestContext.CurrentContext.TestDirectory + "\\";
            var timestamp = DateTime.Now.ToString().Replace(":", "-").Replace(".", "-");
            var fileName = $"TestReport {timestamp}.html";
            var htmlReporter = new ExtentHtmlReporter(dir + fileName);

            extent = new ExtentReports();
            extent.AttachReporter(htmlReporter);

            parser = new PdfParser();

            browser = new Browser();
            browser.Initialize();            
            //LoginPage loginPage = new LoginPage(browser);            
            //loginPage.Login(config.Username, config.Pass);
        }

        [OneTimeTearDown]
        protected void AfterSuit()
        {
            //LoginPage loginPage = new LoginPage(browser);
            //loginPage.Logout();
            browser.Close();
            extent.Flush();

            var json = JsonConvert.SerializeObject(results.ToArray());
            
            var mon = $"{ TestContext.CurrentContext.TestDirectory}\\monitoring.json";
            System.IO.File.WriteAllText(mon, json);

            var testreport = $"{ TestContext.CurrentContext.TestDirectory}\\index.html";
            
            mailClient.SendReport(testreport, config);
        }

        [SetUp]
        public void Init()
        {
            test = extent.CreateTest(TestContext.CurrentContext.Test.Name);            
        }

        [TearDown]
        public void Cleanup()
        {
            Status logstatus;
            var status = TestContext.CurrentContext.Result.Outcome.Status;
            var message = TestContext.CurrentContext.Result.Message;

            if (TestContext.CurrentContext.Result.Outcome != ResultState.Success)
            {
                path = browser.TakeScreenshot(TestContext.CurrentContext.TestDirectory);                
            }
                                 
            switch (status)
            {
                case TestStatus.Failed:
                    logstatus = Status.Fail;
                    test.Log(logstatus, $"Test ended with {logstatus} and message: {message}");
                    test.Log(logstatus, $"Snapshot below: {test.AddScreenCaptureFromPath(path)}");
                    break;
                case TestStatus.Inconclusive:
                    logstatus = Status.Warning;
                    test.Log(logstatus, $"Test ended with {logstatus} and message: {message}");
                    break;
                case TestStatus.Skipped:
                    logstatus = Status.Skip;
                    test.Log(logstatus, $"Test ended with {logstatus} and message: {message}");
                    break;
                default:
                    logstatus = Status.Pass;
                    test.Log(logstatus, $"Test ended with {logstatus}");
                    break;
            }
                        
            results.Add(new MonitoringResult() 
            { 
                Transaction = TestContext.CurrentContext.Test.Name, 
                Status = ConvertStatus(status, message)
            });
                        
        }

        public string ConvertStatus(TestStatus status, string message)
        {
            if(status == TestStatus.Passed)
            {
                return "OK";
            }

            else if(status == TestStatus.Failed && message.Contains("Timed out after"))
            {
                return "IMPACTED";
            }

            return "UNAVAILABLE";
        }

        public string[] ParsePdf(string filePath)
        {
            return parser.GetContent(filePath);
        }        

        public bool AssertPdfContent(string[] lines, List<(string, string)> values)
        {
            int assertionsCount = 0;
            foreach (var line in lines)
            {                
                foreach (var value in values)
                {
                    if (line.StartsWith(value.Item1) && line.Contains(value.Item2))
                    {
                        assertionsCount++;
                    }
                }
            }

            if(assertionsCount == values.Count())
            {
                Console.WriteLine("All expected values were found in the document.");
                return true;
            }
            
            var exception = new AssertionException("Not all expected values were found in the document.");
            throw exception;
        }
    }
}
