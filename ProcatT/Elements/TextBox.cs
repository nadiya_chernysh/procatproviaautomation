﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProcatAutomation.Elements
{
    public class TextBox : Element
    {
        public TextBox(Browser browser, By by) : base(browser, by)
        {

        }

        public void TypeText(string text)
        {
            try
            {
                browser.WaitForElementVisible(bySelect);
                GetWebElement().SendKeys(text);
            }
            catch (ElementNotVisibleException e)
            {
                Logger.Log.Error(e.InnerException);
                throw;
            }
        }

        public void Clear()
        {
            browser.WaitForElementVisible(bySelect);
            GetWebElement().Clear();
        }
    }
}
