﻿using OpenQA.Selenium;
using ProcatAutomation.Elements;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ProcatAutomation.Pages
{
    public class LoginPage : BasePage
    {
        private Browser browser;
        private TextBox userName;
        private TextBox password;
        private Button signIn;
        private Button profile;
        private Button signOut;        
        private Button acceptCookies;

        public LoginPage(Browser browser) : base(browser)
        {
            this.browser = browser;
            userName = new TextBox(browser, By.Id("username"));
            password = new TextBox(browser, By.Id("password"));
            signIn = new Button(browser, By.ClassName("btn--transactional"));
            profile = new Button(browser, By.ClassName("hnf-header__profile-link"));
            signOut = new Button(browser, By.ClassName("profile__link-underline"));
            acceptCookies = new Button(browser, By.Id("onetrust-accept-btn-handler"));
        }        

        public void GoTo()
        {
            browser.Goto("login");
        }

        public void Login(string username, string pw)
        {
            browser.Pause(1);
            userName.TypeText(username);
            browser.Pause(1);
            password.TypeText(pw);
            browser.Pause(1);
            signIn.Click();
            browser.Pause(1);
        }

        public void Logout()
        {
            browser.Pause(1);
            acceptCookies.Click();
            browser.Pause(1);
            profile.Click();            
            browser.Pause(1);
            browser.WaitForElementVisible(By.ClassName("profile__link-underline"));            
            signOut.Click();
            browser.Pause(1);
        }        
    }
}
