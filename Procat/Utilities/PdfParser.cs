﻿using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas.Parser;
using iText.Kernel.Pdf.Canvas.Parser.Listener;
using System.Linq;

namespace E2ETests.Utilities
{
    public class PdfParser
    {
        public string[] GetContent(string filePath)
        {
            PdfReader reader = new PdfReader(filePath);
            PdfDocument pdfDoc = new PdfDocument(reader);
            int pageCount = pdfDoc.GetNumberOfPages();
            string[] lines = null;            

            for (int page = 1; page <= pageCount; page++)
            {
                string currentText = PdfTextExtractor.GetTextFromPage(pdfDoc.GetPage(page), new SimpleTextExtractionStrategy());

                lines = currentText.Split('\n');

                lines.Concat(lines);
            }

            return lines;
        }        
    }
}

