﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E2ETests.Config
{
    public class E2ETestsConfig
    {
        public string Username { get; set; }
        public string Pass { get; set; }
        public string SentFrom { get; set; }
        public string Receivers { get; set; }
        public string Client { get; set; }
        public string ProviaBieter { get; set; }
        public string ProviaBieterPw { get; set; }
        public string DownloadsLocation { get; set; }        
    }
}
