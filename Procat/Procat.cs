using NUnit.Framework;
using ProcatAutomation.Pages;
using E2ETests.Base;

namespace E2ETests
{
    public class Procat : TestBase
    {
        [Test]
        [Ignore("")]
        public void StandardOrderTest()
        {
            CategoriesPage categoriesPage = new CategoriesPage(browser);
            HeaderPage headerPage = new HeaderPage(browser);
            ProductListPage productListPage = new ProductListPage(browser);
            OrderPage orderPage = new OrderPage(browser);
            KontierungSelectionPage kontierungPage = new KontierungSelectionPage(browser);

            categoriesPage.GoToBüromaterialCategory();

            headerPage.SearchForProduct("ordner donau");

            productListPage.AddProductQuantity(10);

            productListPage.AddProductToCart("ordner donau");

            headerPage.OpenCart();

            orderPage.ChangeKontierung();

            kontierungPage.SearchForKostenObjekt(KostenObjekte.ÖBB_Personenverkehr);

            kontierungPage.ConfirmKontierung();

            // ... //          

        }

    }
}
