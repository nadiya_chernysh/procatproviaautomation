﻿using Autofac;
using Autofac.Builder;
using E2ETests.Config;
using Microsoft.Extensions.Configuration;

namespace E2ETests.IocConfig
{
    public static class Ioc
    {
        private static readonly object Lock = new object();

        public static IContainer Container
        {
            get; private set;
        }

        public static void BuildContainer()
        {
            lock (Lock)
            {
                var builder = new ContainerBuilder();

                var config = new ConfigurationBuilder()
                    .AddJsonFile("appsettings.json")
                    .AddEnvironmentVariables()
                    .Build();

                var testsConfig = new E2ETestsConfig();                

                config.GetSection("E2ETests").Bind(testsConfig);                

                builder.Register(c => testsConfig)
                    .SingleInstance();                

                //builder.RegisterModule<E2ETestsModule>();

                Container = builder.Build(ContainerBuildOptions.IgnoreStartableComponents);
            }
        }

        public static void ShutDownContainer()
        {
            Container.Dispose();
        }
    }
}
