﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;
using System.Net.Mime;
using NUnit.Framework;
using E2ETests.Config;
using E2ETests.IocConfig;

namespace E2ETests.Utilities
{    
    public class MailClient
    {        
        public void SendReport(string reportPath, E2ETestsConfig config)
        {           
            var smtpClient = new SmtpClient(config.Client)
            {
                Port = 587,
                Credentials = new NetworkCredential(config.Username, config.Pass),
                EnableSsl = true,
            };

            var mailMessage = new MailMessage
            {
                From = new MailAddress(config.SentFrom),
                Subject = "Test Report",
                Body = $"<p>Please find attached test execution report from a test run finished at {DateTime.Now}.</p>",
                IsBodyHtml = true,
            };
            mailMessage.To.Add(config.Receivers);            
                        
            var attachment = new Attachment(reportPath);
            mailMessage.Attachments.Add(attachment);

            smtpClient.Send(mailMessage);            
        }        
    }    
}
