﻿using OpenQA.Selenium;
using ProcatAutomation.Elements;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProcatAutomation.Pages
{
    public class ProviaVeroffentlicheVerfahrenPage : BasePage
    {
        private Browser browser;
        private Button verfahrenDetails;
        private Button interesseBekunden;
        private Button registrierungLinkAnfordern;
        private Button veroffentlicheVerfahrenTab;
        private TextBox emailInput;
        private Button register;
        private Button absenden;

        public ProviaVeroffentlicheVerfahrenPage(Browser browser) : base(browser)
        {
            this.browser = browser;
            verfahrenDetails = new Button(browser, By.CssSelector("#row0ausschreibungen-grid > div:nth-child(2)"));
            interesseBekunden = new Button(browser, By.CssSelector("#sequence-next-button-title"));
            registrierungLinkAnfordern = new Button(browser, By.CssSelector("#anmeldenForm > div:nth-child(6) > div > div:nth-child(3) > div > button"));            
            veroffentlicheVerfahrenTab = new Button(browser, By.CssSelector("#application-navigation > div > ul > li:nth-child(2) > a"));
            emailInput = new TextBox(browser, By.Id("EmailAddress"));
            register = new Button(browser, By.PartialLinkText("registrieren"));
            absenden = new Button(browser, By.Id("sequence-next-button"));
        }

        public void OpenVerfahrenDetails()
        {
            verfahrenDetails.Click();
            browser.WaitForElementVisible(By.Id("verfahrensdatenSequence"));
        }

        public void ClaimInterest()
        {
            interesseBekunden.Click();
            browser.WaitForElementVisible(By.Id("anmeldenForm"));
        }

        public void RegistrierungLinkAnfordern(string address)
        {
            register.Click();            
            browser.WaitForElementVisible(By.Id("EmailAddress"));            
            emailInput.TypeText(address);            
            absenden.Click();            
        }

        public void WaitForErrorMessage(string errorMessageBlock)
        {            
            browser.WaitForElementVisible(By.ClassName(errorMessageBlock));
        }        

        public void GoTo()
        {
            browser.Goto("https://www.provia.at/bieterportal/VeroeffentlichteElemente");
        }

        public void OpenVerofentlicheVerfahrenTab()
        {
            veroffentlicheVerfahrenTab.Click();
        }
    }
}
