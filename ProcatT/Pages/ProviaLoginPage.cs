﻿using OpenQA.Selenium;
using ProcatAutomation.Elements;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProcatAutomation.Pages
{
    public class ProviaLoginPage : BasePage
    {
        private Browser browser;
        private TextBox userName;
        private TextBox password;
        private Button anmeldenButton;        

        public ProviaLoginPage(Browser browser) : base(browser)
        {
            this.browser = browser;
            userName = new TextBox(browser, By.Id("Benutzername"));
            password = new TextBox(browser, By.Id("Kennwort"));
            anmeldenButton = new Button(browser, By.ClassName("btn-primary"));            
        }

        public void GoTo()
        {
            browser.Goto("Login");
        }

        public void Login(string username, string pw)
        {
            browser.Pause(1);
            userName.TypeText(username);
            browser.Pause(1);
            password.TypeText(pw);
            browser.Pause(1);
            anmeldenButton.Click();
            browser.Pause(1);
            browser.WaitForElementVisible(By.CssSelector("#application-navigation > div > ul > li.active > a"));
        }
    }
}
