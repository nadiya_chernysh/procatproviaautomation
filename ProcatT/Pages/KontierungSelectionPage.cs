﻿using OpenQA.Selenium;
using ProcatAutomation.Elements;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProcatAutomation.Pages
{
    public class KontierungSelectionPage : BasePage
    {
        private Browser browser;        
        private Button mainLogo;
        private Button suche;
        private Button externeShops;
        private Button lieferantenUndKontrakte;
        private Button auswertung;
        private Button produktAnalyse;
        private Button produktRezensionen;
        private Button ausgabeVolumen;
        private Button meineWarenkörbe;
        private Button meineBestellungen;
        private Button warenkorb;
        private TextBox searchField;

        public KontierungSelectionPage(Browser browser) : base(browser)
        {
            this.browser = browser;            
            mainLogo = new Button(browser, By.ClassName("oc-menu-logo__image"));
            suche = new Button(browser, By.LinkText("/opc/seach/index"));
            externeShops = new Button(browser, By.LinkText("/opc/externalShops/index"));
            lieferantenUndKontrakte = new Button(browser, By.LinkText("/opc/browseContracts/index"));
            auswertung = new Button(browser, By.LinkText("/opc/statisticReports/index"));
            produktAnalyse = new Button(browser, By.LinkText("/opc/productAnalysis/index"));
            produktRezensionen = new Button(browser, By.LinkText("/opc/productReviewReport/index"));
            ausgabeVolumen = new Button(browser, By.LinkText("/opc/spendVolumeReport/index"));
            meineWarenkörbe = new Button(browser, By.PartialLinkText("proc/salesOrder/list?user"));
            meineBestellungen = new Button(browser, By.PartialLinkText("proc/purchaseOrder/list?user"));
            searchField = new TextBox(browser, By.Id("q"));
        }

        public void SearchForProduct(string productName)
        {
            searchField.Click();
            searchField.TypeText(productName);
            suche.Click();
            browser.WaitForTextToBePresentInElementLocated(By.ClassName("product-detail-link"), productName);
        }

        public void ConfirmKontierung()
        {
            throw new NotImplementedException();
        }

        public void AddProductQuantity(int quantity)
        {
            searchField.Click();
            searchField.TypeText("");
            suche.Click();
            browser.WaitForTextToBePresentInElementLocated(By.ClassName("product-detail-link"), "");
        }

        public void AddProductToCart(string productName)
        {
            searchField.Click();
            searchField.TypeText("");
            suche.Click();
            browser.WaitForTextToBePresentInElementLocated(By.ClassName("product-detail-link"), "");
        }

        public void SearchForKostenObjekt(KostenObjekte kostenObjekt)
        {
            searchField.Click();
            searchField.TypeText("");
            suche.Click();
            browser.WaitForTextToBePresentInElementLocated(By.ClassName("product-detail-link"), "");
        }
    }
}
