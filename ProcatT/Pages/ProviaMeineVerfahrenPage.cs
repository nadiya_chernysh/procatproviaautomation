﻿using OpenQA.Selenium;
using ProcatAutomation.Elements;
using WindowsInput.Native;
using WindowsInput;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProcatAutomation.Pages
{
    public class ProviaMeineVerfahrenPage : BasePage
    {
        private Browser browser;        
        private Button currentTender;
        private Button angebote;
        private Button verfahrensVerlauf;
        private Button preisangebotErstellen;
        private TextBox grundleistung;
        private TextBox option;
        private TextBox aufschlag;
        private Button weiter;
        private Button dateienHinzufugen;
        private Button ausserhalbSignieren;       
        private Button hauptteilHinzufugen;        
        private Button einreichen;
        private Button saveAndClose;
        private Button userMenu;
        private Button logOut;
        private Button deleteHauptteil;
        private Button orderConfirmation;

        public ProviaMeineVerfahrenPage(Browser browser) : base(browser)
        {
            this.browser = browser;            
            currentTender = new Button(browser, By.CssSelector("#row0grid > div:nth-child(3) > a"));
            angebote = new Button(browser, By.PartialLinkText("Angebote"));
            verfahrensVerlauf = new Button(browser, By.PartialLinkText("Verfahrensverlauf"));
            preisangebotErstellen = new Button(browser, By.Id("btnEinreichen"));
            grundleistung = new TextBox(browser, By.Id("lvsummeohneoption"));
            option = new TextBox(browser, By.Id("option"));
            aufschlag = new TextBox(browser, By.Id("konditionEingabe"));
            weiter = new Button(browser, By.Id("sequence-next-button-title"));
            dateienHinzufugen = new Button(browser, By.ClassName("fileinput-button"));
            ausserhalbSignieren = new Button(browser, By.Id("rbdeckblattsignieren"));
            hauptteilHinzufugen = new Button(browser, By.XPath("/html/body/div[1]/div[4]/div/div/div/div[2]/div[4]/form/div/div[5]/div[2]/div/div[2]/div[6]/div/div[1]/div/div/div[1]/span[1]"));
            einreichen = new Button(browser, By.XPath("/html/body/div[1]/div[4]/div/div/div/div[2]/div[4]/form/div/div[5]/div[2]/div/div[2]/div[6]/div/div[1]/div/div/div[1]/span[1]"));
            saveAndClose = new Button(browser, By.CssSelector("#sequence-next-button-title"));            
            userMenu = new Button(browser, By.CssSelector("#system-navigation > div.navigation-responsive-collapse.navbar-collapse.collapse.hidden-print > ul > li:nth-child(2) > a > b"));
            logOut = new Button(browser, By.PartialLinkText("Abmelden"));
            deleteHauptteil = new Button(browser, By.XPath("/html/body/div[1]/div[4]/div/div/div/div[2]/div[4]/form/div/div[5]/div[2]/div/div[2]/div[6]/div/div[1]/div/div/div[4]/table/tbody/tr/td[5]/button"));
            orderConfirmation = new Button(browser, By.CssSelector("img[title='Angebotsbestaetigung.pdf']"));
        }

        public void OpenCurrentTender()
        {
            browser.WaitForElementVisible(By.CssSelector("#row0grid > div:nth-child(3) > a"));
            currentTender.Click();
            browser.WaitForElementClickable(By.PartialLinkText("Angebote"));
        }

        public string GetCurrentTenderId()
        {
            string url = browser.GetCurrentUrl();
            var id = url.Split('/')[5];
            return id;
        }

        public void GoToAngeboteSection()
        {
            angebote.Click();
            browser.WaitForElementClickable(By.Id("btnEinreichen"));
        }

        public void GoToVerfahrensverlauf()
        {
            verfahrensVerlauf.Click();
            browser.WaitForElementClickable(By.PartialLinkText("Datum"));
        }

        public void PreisangebotErstellen()
        {
            preisangebotErstellen.Click();
            browser.WaitForElementClickable(By.ClassName("angebotExpander"));
        }

        public void FillBasisangebotData(int grundleistungData, int optionData, int aufschlagData)
        {            
            grundleistung.Clear();
            browser.Pause(1);
            grundleistung.Click();
            ClearFieldManually();
            grundleistung.TypeText(grundleistungData.ToString());
            browser.Pause(1);
            option.Clear();
            option.Click();
            ClearFieldManually();
            option.TypeText(optionData.ToString());
            browser.Pause(1);
            aufschlag.Clear();
            aufschlag.Click();
            ClearFieldManually();            
            aufschlag.TypeText(aufschlagData.ToString());
            option.Click();
            //weiter.Click();            
        }

        public void UploadBestandteile()
        {
            weiter.Click();
            browser.WaitForElementClickable(By.ClassName("fileinput-button"));
            browser.Pause(1);            
            dateienHinzufugen.Click();
            browser.Pause(1);
            UploadFile();
            browser.WaitForElementClickable(By.ClassName("btn-danger"));
            browser.WaitForElementClickable(By.PartialLinkText("ProVia-Impressum.pdf"));
            weiter.Click();
            browser.Pause(1);
        }

        public void UploadFile()
        {
            InputSimulator simulator = new InputSimulator();
            simulator.Keyboard.Sleep(1000)
                .TextEntry("C:\\ÖBB\\Procat\\ProcatT\\ProVia-Impressum.pdf")
                .Sleep(1000)
                .KeyPress(VirtualKeyCode.RETURN);
        }

        public void ClearFieldManually()
        {
            InputSimulator simulator = new InputSimulator();
            simulator.Keyboard.Sleep(1000)
                .KeyPress(VirtualKeyCode.DELETE)
                .KeyPress(VirtualKeyCode.DELETE)
                .KeyPress(VirtualKeyCode.DELETE)
                .KeyPress(VirtualKeyCode.DELETE)
                .KeyPress(VirtualKeyCode.DELETE)                
                .KeyPress(VirtualKeyCode.DELETE);
        }

        public void SelectSignature(bool outsideOfPlatform = true)
        {
            browser.Pause(1);
            browser.WaitForElementClickable(By.Id("rbdeckblattsignieren"));
            browser.Pause(1);
            if (outsideOfPlatform)
            {
                ausserhalbSignieren.Click();
            }            
        }

        public void UploadAngebotshauptteil()
        {            
            hauptteilHinzufugen.Click();
            browser.Pause(1);
            UploadFile();            
            browser.WaitForElementClickable(By.PartialLinkText("ProVia-Impressum.pdf"));
        }

        public void SubmitOffer()
        {           
            einreichen.Click();
        }

        public void SaveOfferAndClose()
        {
            browser.Pause(1);
            saveAndClose.Click();
            browser.Pause(1);
            browser.AcceptAlert();
        }

        public void Logout()
        {
            userMenu.Click();
            logOut.Click();
        }

        public void DeleteAngebotsHauptteil()
        {
            browser.WaitForElementClickable(By.PartialLinkText("Angebote"));
            browser.Pause(2);
            preisangebotErstellen.Click();
            browser.WaitForElementClickable(By.ClassName("angebotExpander"));
            weiter.Click();
            browser.WaitForElementClickable(By.PartialLinkText("ProVia-Impressum.pdf"));
            weiter.Click();            
            browser.WaitForElementNotVisible(By.ClassName("ui-widget-overlay"));
            browser.Pause(2);
            deleteHauptteil.Click();
        }

        public void VerifyGesamtpreis(string expectedGesamtpreis)
        {
            browser.WaitForTextToBePresentInElementLocated(By.CssSelector("#anggebotEinreichenSequence > div.sequence-step.sequence-static-content-loaded.sequence-step-active > div:nth-child(1) > div > div.panel-body > table > tbody > tr:nth-child(3) > td:nth-child(4) > p"), expectedGesamtpreis);
        }

        public void DownloadOfferConfirmation()
        {
            orderConfirmation.Click();
            browser.Pause(1);
        }
    }
}
