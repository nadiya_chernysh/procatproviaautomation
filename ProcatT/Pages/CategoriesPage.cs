﻿using OpenQA.Selenium;
using ProcatAutomation.Elements;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProcatAutomation.Pages
{
    public class CategoriesPage : BasePage
    {
        private Browser browser;
        private Button orderList;
        private Button profile;
        private Button mainLogo;
        private Button uniformen;
        private Button psa;
        private Button kopfschutz;
        private Button augenschutz;
        private Button schutzkleidung;
        private Button büromaterial;
        private Button handyUndTablet;
        private Button freitex;
        private Button itProdukte;
        private Button mdfClients;
        private Button bücher;
        private Button stempel;
        private Button userWarenkörbe;
        private Button userBestellungen;

        public CategoriesPage(Browser browser) : base(browser)
        {
            this.browser = browser;            
            orderList = new Button(browser, By.ClassName("ahnf-header__shopping-list-link"));
            profile = new Button(browser, By.ClassName("hnf-header__profile-link"));
            mainLogo = new Button(browser, By.ClassName("oc-menu-logo__image"));
            uniformen = new Button(browser, By.PartialLinkText("3ABC_008"));
            psa = new Button(browser, By.PartialLinkText("3ABC_007"));
            kopfschutz = new Button(browser, By.PartialLinkText("3ABC_0071"));
            augenschutz = new Button(browser, By.PartialLinkText("3ABC_0072"));
            schutzkleidung = new Button(browser, By.PartialLinkText("3ABC_0075"));
            büromaterial = new Button(browser, By.PartialLinkText("3ABC_0075"));
            handyUndTablet = new Button(browser, By.PartialLinkText("3ABC_006"));
            freitex = new Button(browser, By.PartialLinkText("3ABC_010"));
            itProdukte = new Button(browser, By.PartialLinkText("3ABC_011"));
            mdfClients = new Button(browser, By.PartialLinkText("3ABC_004"));
            bücher = new Button(browser, By.PartialLinkText("3ABC_002"));
            stempel = new Button(browser, By.PartialLinkText("3ABC_009"));
            userWarenkörbe = new Button(browser, By.PartialLinkText("proc/salesOrder/list?user"));
            userBestellungen = new Button(browser, By.PartialLinkText("proc/purchaseOrder/list?user"));
        }

        public void GoTo()
        {
            browser.Goto("opc/search");
        }

        public void OpenOrderList()
        {            
            orderList.Click();            
            browser.WaitForElementVisible(By.ClassName("print-button"));            
        }

        public void GoToBüromaterialCategory()
        {
            büromaterial.Click();
            browser.WaitForElementVisible(By.ClassName("searchResultContainer"));
        }

        public void FilterOrderByStatus()
        {

        }

        public void ConfirmLastOrder() 
        {
            
        }


        public void OpenProfile()
        {
            browser.Pause(1);
            profile.Click();
            browser.Pause(1);            
        }
        public void Logout()
        {            
            //browser.WaitForElementVisible(By.LinkText("/at/de/profile/logout/"));
            //browser.Pause(1);
        }

    }
}
